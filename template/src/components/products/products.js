var products;

products = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    varsInit();
    sliderMake();
  };

  var varsInit = function () {
    $el = $(".products");
    $slider = $el.find('.products__slider');
  };

  var sliderMake = function () {
    $el.imagesLoaded({background: true}).done(function () {
      $slider.slick({
        arrows: false,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        vertical: false,
        cssEase: 'linear',
        draggable: false,
        responsive: [{
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
    });
  };
};