var header;

header = new function () {

  //catch DOM
  var $el;
  var $panelIcons;

  //private vars
  var documentTop;
  var breakPosition = false;

  //bind events
  $(document).ready(function () {
    init();
    watchPanelItems();
    makeSpaceAfter();
  });

  $(window).scroll(function () {
    setTimeout(function () {
      documentTop = $(document).scrollTop();

      if (documentTop > 60 && !breakPosition) {
        breakPosition = true;
        $el.addClass('-scroll');
      } else if (documentTop < 100 && breakPosition) {
        breakPosition = false;
        $el.removeClass('-scroll');
      }

      resetPanelItems();
    }, 0);
  });

  //private functions
  var init = function () {
    $el = $('.header');
    $panelIcons = $('.header__panelIcon');
  };

  var watchPanelItems = function () {
    $panelIcons.on('click', function () {

      var $this = $(this);
      var $thisContent = $this.parent().find('.header__panelContent');

      if ($this.hasClass('-active')) {
        resetPanelItems();
      } else {
        resetPanelItems();
        $this.addClass('-active');
        $thisContent.addClass('-show');
      }
    });
  };

  var resetPanelItems = function () {
    $panelIcons.removeClass('-active');
    $panelIcons.parent().find('.header__panelContent').removeClass('-show');
  };

  var makeSpaceAfter = function () {
    $el.after('<div class="header__afterSpace"></div>').log;
  };
};
