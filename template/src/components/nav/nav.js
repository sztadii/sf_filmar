var nav;

nav = new function () {

  //catch DOM
  var $el;
  var $nav;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    varsInit();
    makeAccordion();
  };

  var varsInit = function () {
    $el = $('.nav');
    $nav = $el.find('.nav__list');
  };

  var makeAccordion = function () {
    hideList($nav.not('.-main'));

    $nav.bind('click', function (event) {
      var $this = $(this);

      event.stopPropagation();
      if ($this.hasClass('-active')) {
        hideList($this);
        hideList($this.find('.nav__list.-active'));
      } else {
        var $another = $this.parent().siblings().find('.nav__list.-active');

        hideList($another);
        showList($this);
      }
    });
  };

  var hideList = function (list) {
    list.removeClass('-active').children().slideUp()
  };

  var showList = function (list) {
    list.addClass('-active').children().slideDown(400);
  }
};
