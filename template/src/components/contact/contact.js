var offer;

offer = new function () {

  //catch DOM
  var $el;
  var $button;
  var $form;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    varsInit();
    watchButton();
  };

  var varsInit = function () {
    $el = $(".contact");
    $button = $el.find('.contact__button');
    $form = $el.find('.contact__form');
  };

  var watchButton = function () {
    var isOpening = false;

    $form.hide();

    $button.bind('click', function () {
      if (!isOpening) {

        isOpening = true;
        $(this).toggleClass('-active');

        $form.slideToggle(function () {
          isOpening = false;
        });
      }
    });
  }
};