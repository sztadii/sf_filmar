var loader;

loader = new function () {

  //catch DOM
  var $el;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".loader");

    $el.imagesLoaded({background: true}).done(function () {
      start();
    });
  };

  var start = function () {
    setTimeout(function () {
      $el.addClass('-hide');

      setTimeout(function () {
        $el.remove();
      }, 1000);
    }, 1000);
  };
};