var banner;

banner = new function () {

  //private vars
  var height;
  var width;

  //catch DOM
  var $el;
  var $slider;
  var $sliderItems;
  var $sliderBox;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).bind('resize', function () {

    if ($(window).width() != width) {
      heightChange();
    }
  });

  //private functions
  var init = function () {
    varsInit();
    sliderMake();
    heightChange();
  };

  var varsInit = function () {
    $el = $('.banner.-main');
    $slider = $el.find('.banner__slider');
    $sliderItems = $slider.find('.banner__item');
    $sliderBox = $slider.find('.banner__box');
  };

  var sliderMake = function () {
    if ($slider.length > 0) {
      $slider.imagesLoaded({background: true}).done(function () {

        $slider.slick({
          infinite: true,
          dots: false,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: false,
          adaptiveHeight: true,
          autoplay: true,
          speed: 600,
          autoplaySpeed: 4000,
          draggable: false,
          swipe: false,
          pauseOnHover: false,
          pauseOnFocus: false
        });

        $slider.on('beforeChange', function () {
          $sliderBox.addClass('-hide');
        });

        $slider.on('afterChange', function () {
          $sliderBox.removeClass('-hide');
        });
      });
    }
  };

  var heightChange = function () {
    height = $(window).height() - $('.header').height();
    width = $(window).width();

    $sliderItems.css({
      'height': height
    });
  };
};
