var offer;

offer = new function () {

  //catch DOM
  var $el;
  var $slider;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    varsInit();
    sliderMake();
  };

  var varsInit = function () {
    $el = $(".offer");
    $slider = $el.find('.offer__slider');
  };

  var sliderMake = function () {
    $el.imagesLoaded({background: true}).done(function () {
      $slider.slick({
        arrows: true,
        appendArrows: $('.offer__arrows'),
        prevArrow: '<img class="offer__arrow -left" src="resources/img/offer__arrow-left.png" alt="arrow">',
        nextArrow: '<img class="offer__arrow -right" src="resources/img/offer__arrow-right.png" alt="arrow">',
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        dotsClass: 'slick__dots',
        appendDots: $('.offer__dots'),
        customPaging: function (a, b) {
          return '<div class="slick__dot"></div>';
        },
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        vertical: false,
        cssEase: 'linear',
        draggable: false,
        responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }, {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        ]
      });
    });
  };
};